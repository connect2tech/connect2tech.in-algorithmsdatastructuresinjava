package in.connect2tech.sorting;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/
 * @LinkedIn : https://www.linkedin.com/in/naresh-chaurasia/
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class SelectionSort {

	private static int numberOfElements = 6;
	private static int[] elements = { 25, 47, 3, 19, 8, 18 };

	public static void main(String[] args) {

		System.out.println("Elements before sorting...");
		for (int i = 0; i < elements.length; i++) {
			System.out.print(elements[i] + "	");
		}
		System.out.println("");
		System.out.println("--------------------------------------");
		selectionSort();

		System.out.println("Elements after sorting...");
		for (int j = 0; j < elements.length; j++) {
			System.out.print(elements[j] + "	");
		}
	}

	public static void selectionSort() {
		int out, in, min;
		for (out = 0; out < numberOfElements - 1; out++) // outer loop
		{
			min = out; // minimum
			for (in = out + 1; in < numberOfElements; in++) // inner loop
				if (elements[in] < elements[min]) // if min greater,
					min = in; // we have a new min
			swap(out, min); // swap them
		} // end for(out)
	} // end selectionSort()

	private static void swap(int one, int two) {
		int temp = elements[one];
		elements[one] = elements[two];
		elements[two] = temp;
	}
}
