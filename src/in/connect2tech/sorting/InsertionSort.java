package in.connect2tech.sorting;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/
 * @LinkedIn : https://www.linkedin.com/in/naresh-chaurasia/
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class InsertionSort {

	private static int[] elements = { 25, 47, 3, 19, 8, 18 };

	public static void main(String[] args) {

		System.out.println("Elements before sorting...");
		for (int i = 0; i < elements.length; i++) {
			System.out.print(elements[i] + "	");
		}
		System.out.println("");
		System.out.println("--------------------------------------");
		insertionSort();

		System.out.println("Elements after sorting...");
		for (int j = 0; j < elements.length; j++) {
			System.out.print(elements[j] + "	");
		}
	}

	public static void insertionSort() {
		{
			int n = elements.length;
			for (int i = 1; i < n; ++i) {
				int key = elements[i];
				int j = i - 1;

				/*
				 * Move elements of arr[0..i-1], that are greater than key, to
				 * one position ahead of their current position
				 */
				while (j >= 0 && elements[j] > key) {
					elements[j + 1] = elements[j];
					j = j - 1;
				}
				elements[j + 1] = key;
			}
		}
	}

}
