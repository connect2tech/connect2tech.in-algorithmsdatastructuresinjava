package in.connect2tech.sorting;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/
 * @LinkedIn : https://www.linkedin.com/in/naresh-chaurasia/
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class MergeSort {
	public static void main(String[] args) {
		int[] arrayA = { 23, 47, 81, 95 };
		int[] arrayB = { 7, 14, 39, 55, 62, 74 };
		int[] arrayC = new int[10];

		merge(arrayA, 4, arrayB, 6, arrayC);
		display(arrayC, 10);
	} // end main()
		// -----------------------------------------------------------
		// merge A and B into C

	public static void merge(int[] arrayA, int sizeA, int[] arrayB, int sizeB, int[] arrayC) {
		int aCounter = 0, bCounter = 0, cCounter = 0;

		while (aCounter < sizeA && bCounter < sizeB) // neither array empty
			if (arrayA[aCounter] < arrayB[bCounter])
				arrayC[cCounter++] = arrayA[aCounter++];
			else
				arrayC[cCounter++] = arrayB[bCounter++];

		while (aCounter < sizeA) // arrayB is empty,
			arrayC[cCounter++] = arrayA[aCounter++]; // but arrayA isn't

		while (bCounter < sizeB) // arrayA is empty,
			arrayC[cCounter++] = arrayB[bCounter++]; // but arrayB isn't
	} // end merge()

	public static void display(int[] theArray, int size) {
		for (int j = 0; j < size; j++)
			System.out.print(theArray[j] + " ");
		System.out.println("");
	}
	// -----------------------------------------------------------
}
