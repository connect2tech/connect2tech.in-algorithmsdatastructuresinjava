package in.connect2tech.bigo;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/
 * @LinkedIn : https://www.linkedin.com/in/naresh-chaurasia/
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class CountEvenNumbersInArray {

	int countEvenNumbers(int[] elements) {
		int count = 0;

		for (int i = 0; i < elements.length; i++) {
			if (elements[i] % 2 == 0) {
				++count;
			}
		}
		return count;
	}
}
