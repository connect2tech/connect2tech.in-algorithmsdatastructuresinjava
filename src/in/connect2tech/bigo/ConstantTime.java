package in.connect2tech.bigo;

/**
 * @author : Naresh Chaurasia
 * @email : message4naresh@gmail.com
 * @Code : https://bitbucket.org/connect2tech
 * @WebSite : https://connect2tech.in/
 * @LinkedIn : https://www.linkedin.com/in/naresh-chaurasia/
 * @YouTubeChannel: https://goo.gl/c7FAsq
 */

public class ConstantTime {

	public int getElementFromArray(int[] arr, int index) {
		return arr[index];
	}
}
